<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $DB;

$templateFolder = $this->GetFolder();

$APPLICATION->AddHeadString('<link href="' . $templateFolder."/css/qmn_primary.css" .'" type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadString('<link href="' . $templateFolder."/css/common.css" .'" type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadString('<link href="' . $templateFolder."/css/jquery-ui.css" .'" type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadString('<link href="' . $templateFolder."/css/modal.css" .'" type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadString('<link href="' . $templateFolder."/css/jquery.ui.slider-rtl.css" .'" type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadScript($templateFolder."/js/underscore-min.js");
$APPLICATION->AddHeadScript($templateFolder."/js/progressbar.min.js");
//$APPLICATION->AddHeadScript($templateFolder."/js/jquery.js");
$APPLICATION->AddHeadScript($templateFolder."/js/jquery-ui.js");
$APPLICATION->AddHeadScript($templateFolder."/js/tooltip.min.js");
$APPLICATION->AddHeadScript($templateFolder."/js/qsm-quiz.js");
$APPLICATION->AddHeadScript($templateFolder."/js/jquery.inputmask.min.js");

$rfnQuizzes = $DB->Query("SELECT * FROM rfn_quizzes;");
while ($quiz = $rfnQuizzes->Fetch())
{
    $QuizId = $quiz['quiz_id'];

    $rfnQuestions = $DB->Query("SELECT * FROM rfn_questions WHERE quiz_id = {$QuizId};");
    $questions = array ();
    $questionsList = array ();
    $totalQuestions = $rfnQuestions->AffectedRowsCount();
    while ($question = $rfnQuestions->Fetch())
    {
        $questions [] = $question;
        $questionsList [] = "{$question['question_id']}Q";
    }

    $QuizName = $quiz['quiz_name'];
    $settings = json_decode($quiz['quiz_settings']);
    $contactsText = $settings ? $settings->contactsText : null;

    $conditions = array ();
    $rfnConditions = $DB->Query("SELECT * FROM rfn_conditions WHERE quiz_id = {$QuizId};");
    while ($condition = $rfnConditions->Fetch())
    {
        if ($condition) {
            $condition['condition_array'] = json_decode($condition['condition_array'], true);
            $conditions [] = $condition;
        }
    }

    $qmn_json_data = array(
        'quiz_id' => $QuizId,
        'quiz_name' => $QuizName,
        'disable_answer' => 0,
        'ajax_show_correct' => 0,
        'progress_bar' => 1,
        'error_messages' => array(
            'email' => 'Not a valid e-mail address!',
            'number' => 'This field must be a number!',
            'incorrect' => 'The entered text is not correct!',
            'empty' => 'Please complete all required fields',
        ),
        'conditions' => $conditions
    );

    ?>

    <div data-quiz-id="<?= $QuizId ?>" class="quiz-modal">
        <div class="quiz-modal-content">
            <span class="quiz-modal-close">&times;</span>
            <script>
                if (window.qmn_quiz_data === undefined) {
                    window.qmn_quiz_data = new Object();
                }
            </script>
            <div class='qsm-quiz-container qmn_quiz_container mlw_qmn_quiz'>
                <form name='quizForm<?= $QuizId ?>' data-quiz-id='<?= $QuizId ?>' id='quizForm<?= $QuizId ?>' action='' method='POST' class='qsm-quiz-form qmn_quiz_form mlw_quiz_form' novalidate  enctype='multipart/form-data'>
                    <div style='display: none' name='mlw_error_message' id='mlw_error_message' class='qsm-error-message qmn_error_message_section'></div>
                    <span id='mlw_top_of_quiz'></span>
                    <div class="quiz_section">
                        <div class='qsm-quiz-description mlw_qmn_quiz_description'>
                            <?= $QuizName ?>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                    <div class="qsm-progress-percents" style="display:none;">Готово: <span class="percents-text"><span class="percents-value">0</span>%</span></div>
                    <div id="pgb<?= $QuizId ?>" class="qsm-progress-bar" style="display:none;"></div>
                    <?php foreach ($questions as $question) {

                    $QuestionId = $question['question_id'];
                    $QuestionName = $question['question_name'];
                    $QuestionType = $question['question_type'];
                    $answers = json_decode($question['answer_array']);

                    ?>

                    <section class="qsm-page">
                        <div class='quiz_section question-section-id-<?= $QuestionId ?>'>
                            <span class='mlw_qmn_question quiz-question-title ' ><?= $QuestionName ?></span>
                            <?php switch ($QuestionType) {
                                case 0: // Единичный выбор ?>

                            <div class='qmn_radio_answers mlwRequiredRadio'>
                                <?php foreach ($answers as $AnswerIndex => $answer) {
                                    $AnswerText = $answer->AnswerText;
                                    $AnswerOwn = $answer->AnswerOwn;
                                    $AnswerImageUrl = $answer->AnswerImageUrl;
                                ?>

                                <label for='question<?= $QuestionId ?>_<?= $AnswerIndex ?>' class='qmn_mc_answer_wrap' id='question<?= $QuestionId ?>-<?= $AnswerText ?>'>
                                    <div class='qmn_mc_answer_answer_border'>

                                        <input data-own='<?= $AnswerOwn ? 1 : '' ?>' type='radio' class='qmn_quiz_radio' name='question<?= $QuestionId ?>' id='question<?= $QuestionId ?>_<?= $AnswerIndex ?>' value='<?= $AnswerText ?>' />
                                        <?php if ($AnswerOwn) { ?>
                                            <input class="own-answer" type="text" value="" placeholder="<?= $AnswerText ? $AnswerText : 'Свой вариант' ?>">
                                        <?php } else { ?>
                                            <span>
                                                <?php if ($AnswerImageUrl) { ?>
                                                    <img src="<?= $AnswerImageUrl ?>" />
                                                <?php } ?>
                                                <?= $AnswerText ?>
                                            </span>
                                        <?php } ?>
                                    </div>
                                </label>

                                <?php } ?>

                                <input type='radio' style='display: none;' name='question<?= $QuestionId ?>' id='question<?= $QuestionId ?>_none' checked='checked' value='No Answer Provided' />
                            </div>
                                <?php
                                break;
                            case 3: // Текст ?>

                            <input  type='text' class='mlw_answer_open_text mlwRequiredText' name='question<?= $QuestionId ?>' />
                                <?php
                                break;
                            case 4: // Множественный выбор ?>

                            <div class='qmn_check_answers mlwRequiredCheck'>

                                <?php foreach ($answers as $AnswerIndex => $answer) {
                                    $AnswerText = $answer->AnswerText;
                                    $AnswerOwn = $answer->AnswerOwn;
                                    $AnswerImageUrl = $answer->AnswerImageUrl;
                                    ?>

                                <label for="question<?= $QuestionId ?>_<?= $AnswerIndex ?>" class="qsm_check_answer">
                                    <div class="qmn_mc_answer_answer_border">
                                        <input type='checkbox' name='question<?= $QuestionId ?>_<?= $AnswerIndex ?>' id='question<?= $QuestionId ?>_<?= $AnswerIndex ?>' value='<?= $answer->AnswerText || $AnswerOwn ? $answer->AnswerText : $AnswerImageUrl ?>' />
                                        <?php if ($AnswerOwn) { ?>
                                            <input class="own-answer" type="text" value="" placeholder="<?= $AnswerText ? $AnswerText : 'Свой вариант' ?>">
                                        <?php } else { ?>
                                            <?php if ($AnswerImageUrl) { ?>
                                                <div class='answer-checked'>✔</div>
                                                <img src="<?= $AnswerImageUrl ?>" />
                                            <?php } ?>
                                            <span><?= $AnswerText ?></span>
                                        <?php } ?>
                                    </div>
                                </label>

                                <?php } ?>

                            </div>
                                <?php
                                break;
                            case 7: // Число ?>

                            <input type='number' class='mlw_answer_number mlwRequiredNumber' name='question<?= $QuestionId ?>' />
                                <?php
                                break;
                            } ?>

                        </div>
                    </section>
                    <?php } ?>

                    <section class="qsm-page">
                        <div class="quiz_section">
                            <div class='qsm-after-message mlw_qmn_message_end'>
                                <?= $contactsText ?>
                            </div>
                            <div class="qsm_contact_div qsm-contact-type-text">
                                <div class="input-group contact-group contact-group-bg-white">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                                        <path fill="#e3e3e3" d="M20.822 18.096c-3.439-.794-6.64-1.49-5.09-4.418 4.72-8.912 1.251-13.678-3.732-13.678-5.082 0-8.464 4.949-3.732 13.678 1.597 2.945-1.725 3.641-5.09 4.418-3.073.71-3.188 2.236-3.178 4.904l.004 1h23.99l.004-.969c.012-2.688-.092-4.222-3.176-4.935z"/>
                                    </svg>
                                    </span>
                                    </div>
                                    <input  type='text' class='form-control mlwRequiredText qsm_required_text' x-webkit-speech name='contact_field_0' value='' placeholder="Имя" />
                                </div>
                            </div>
                            <div class="qsm_contact_div qsm-contact-type-text">
                                <div class="input-group contact-group contact-group-bg-gray">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                        <span style="padding: 0 7px;">+7</span>
                                    </span>
                                    </div>
                                    <input  type='text' class='form-control mlwRequiredNumber phone-mask qsm_required_text' x-webkit-speech name='contact_field_1' value='' placeholder="Телефон" />
                                </div>
                            </div>
                        </div>
                    </section>

                    <input type='hidden' name='qmn_question_list' value='<?= implode('', $questionsList) ?>' />
                    <div style='display: none' name='mlw_error_message_bottom' id='mlw_error_message_bottom' class='qsm-error-message qmn_error_message_section'></div>
                    <input type='hidden' name='total_questions' id='total_questions' value='<?= $totalQuestions ?>'/>
                    <input type='hidden' name='timer' id='timer' value='0'/>
                    <input type='hidden' name='timer_ms' id='timer_ms' value='0'/>
                    <input type='hidden' class='qmn_quiz_id' name='qmn_quiz_id' id='qmn_quiz_id' value='<?= $QuizId ?>'/>
                    <input type='hidden' name='complete_quiz' value='confirmation' />
                </form>
            </div>
            <script>
                window.qmn_quiz_data["<?= $qmn_json_data["quiz_id"] ?>"] = <?= json_encode($qmn_json_data) ?>
            </script>
        </div>
    </div>
    <?php
}

?>

<script type="text/template" id="tmpl-qsm-pagination">
    <div class="qsm-pagination qmn_pagination">
        <a class="qsm-btn qsm-previous qmn_btn mlw_qmn_quiz_link mlw_previous" href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>
            <span style="margin-left: 5px;">Назад</span>
        </a>
        <span class="qmn_page_message"></span>
        <div class="qmn_page_counter_message"></div>
        <a class="qsm-btn qsm-next qmn_btn mlw_qmn_quiz_link mlw_next" href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24 24"><path fill="white" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/></svg>
            <span style="margin-left: 5px;">Далее</span>
        </a>
        <button type='submit' class='qsm-btn qsm-submit-btn qmn_btn'>
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path fill="white" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/></svg>
            <span style="margin-left: 8px;">
                Получить результаты
            </span>
        </button>
    </div>
</script>
